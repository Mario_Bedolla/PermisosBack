package com.forte.permisos.controllers;

import com.forte.permisos.dto.request.PermisoRequest;
import com.forte.permisos.services.IPermisoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200/", maxAge = 3600)
@RestController
@RequestMapping("permiso")
public class PermisoController {

    @Autowired
    @Qualifier("permisoServiceImpl")
    private IPermisoService iPermisoService;

    @RequestMapping(value="addPermiso", method = RequestMethod.POST)
    public ResponseEntity<?> addPermiso(@Validated @RequestBody PermisoRequest permisoRequest){
        return ResponseEntity.ok(iPermisoService.addPermiso(permisoRequest));
    }

    @RequestMapping(value="getAllPermisos", method = RequestMethod.GET)
    public ResponseEntity<?> getAllPermisos(){
       return ResponseEntity.ok(iPermisoService.permisoResponseList());
    }

    @RequestMapping(value="deletePermiso/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id){
        iPermisoService.deletePermiso(id);
    }

    @RequestMapping(value="getAllTipoPermiso", method = RequestMethod.GET)
    public ResponseEntity<?> getAllTipoPermiso(){
        return ResponseEntity.ok(iPermisoService.tipoPermisoList());
    }
}
