package com.forte.permisos.dto.request;

import java.util.Date;

public class PermisoRequest {

    private Long id;
    private String nombreEmpleado;
    private String apellidosEmpleado;
    private Long tipoPermisoId;
    private Date fechaPermisoDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getApellidosEmpleado() {
        return apellidosEmpleado;
    }

    public void setApellidosEmpleado(String apellidosEmpleado) {
        this.apellidosEmpleado = apellidosEmpleado;
    }

    public Long getTipoPermisoId() {
        return tipoPermisoId;
    }

    public void setTipoPermisoId(Long tipoPermisoId) {
        this.tipoPermisoId = tipoPermisoId;
    }

    public Date getFechaPermisoDate() {
        return fechaPermisoDate;
    }

    public void setFechaPermisoDate(Date fechaPermisoDate) {
        this.fechaPermisoDate = fechaPermisoDate;
    }
}
