package com.forte.permisos.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PERMISO")
public class PermisoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column
    private String nombreEmpleado;

    @Column
    private String apellidosEmpleado;

    @Column
    private Date fechaPermiso;

    @ManyToOne
    @JoinColumn(name = "tipo_permiso_id")
    private TipoPermisoEntity tipoPermiso;

    public PermisoEntity() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getApellidosEmpleado() {
        return apellidosEmpleado;
    }

    public void setApellidosEmpleado(String apellidosEmpleado) {
        this.apellidosEmpleado = apellidosEmpleado;
    }

    public Date getFechaPermiso() {
        return fechaPermiso;
    }

    public void setFechaPermiso(Date fechaPermiso) {
        this.fechaPermiso = fechaPermiso;
    }

    public TipoPermisoEntity getTipoPermiso() {
        return tipoPermiso;
    }

    public void setTipoPermiso(TipoPermisoEntity tipoPermiso) {
        this.tipoPermiso = tipoPermiso;
    }
}
