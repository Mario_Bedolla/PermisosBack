package com.forte.permisos.repository;

import com.forte.permisos.entity.PermisoEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermisoRepository extends JpaRepository<PermisoEntity,Long> {
    List<PermisoEntity> findAll(Sort sort);
}
