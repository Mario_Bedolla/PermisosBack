package com.forte.permisos.repository;

import com.forte.permisos.entity.TipoPermisoEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TipoPermisoRepository extends JpaRepository<TipoPermisoEntity,Long> {

}
