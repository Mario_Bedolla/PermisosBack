package com.forte.permisos.services;

import com.forte.permisos.dto.request.PermisoRequest;
import com.forte.permisos.dto.response.PermisoResponse;
import com.forte.permisos.entity.TipoPermisoEntity;

import java.util.List;

public interface IPermisoService {
    List<PermisoResponse> permisoResponseList ();
    PermisoResponse addPermiso (PermisoRequest permisoRequest);
    void deletePermiso (Long id);

    List<TipoPermisoEntity> tipoPermisoList();
}
