package com.forte.permisos.services.impl;

import com.forte.permisos.dto.request.PermisoRequest;
import com.forte.permisos.dto.response.PermisoResponse;
import com.forte.permisos.entity.PermisoEntity;
import com.forte.permisos.entity.TipoPermisoEntity;
import com.forte.permisos.repository.PermisoRepository;
import com.forte.permisos.repository.TipoPermisoRepository;
import com.forte.permisos.services.IPermisoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class PermisoServiceImpl implements IPermisoService {

    @Autowired
    private PermisoRepository permisoRepository;
    @Autowired
    private TipoPermisoRepository tipoPermisoRepository;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public List<PermisoResponse> permisoResponseList() {
        List<PermisoEntity> permisoEntityList;
        List<PermisoResponse> permisoResponseList = new ArrayList<>();

        permisoEntityList = permisoRepository.findAll();

        permisoEntityList.forEach(permiso -> {
            PermisoResponse permisoResponse = new PermisoResponse();
            permisoResponse.setId(permiso.getId());
            permisoResponse.setNombreEmpleado(permiso.getNombreEmpleado());
            permisoResponse.setApellidosEmpleado(permiso.getApellidosEmpleado());
            permisoResponse.setFechaPermiso(dateFormat.format(permiso.getFechaPermiso()));
            permisoResponse.setTipoPermiso(permiso.getTipoPermiso().getDescripcion());
            permisoResponse.setTipoPermisoId(permiso.getTipoPermiso().getId());
            permisoResponseList.add(permisoResponse);
        });
        return permisoResponseList;
    }

    @Override
    public PermisoResponse addPermiso(PermisoRequest permisoRequest) {
        PermisoEntity permisoEntity = new PermisoEntity();
        if (permisoRequest.getId()!=null){
            permisoEntity.setId(permisoRequest.getId());
        }
        TipoPermisoEntity tipoPermiso = tipoPermisoRepository.getById(permisoRequest.getTipoPermisoId());
        permisoEntity.setFechaPermiso(permisoRequest.getFechaPermisoDate());
        permisoEntity.setTipoPermiso(tipoPermiso);
        permisoEntity.setApellidosEmpleado(permisoRequest.getApellidosEmpleado());
        permisoEntity.setNombreEmpleado(permisoRequest.getNombreEmpleado());

        permisoEntity = permisoRepository.save(permisoEntity);

        PermisoResponse permisoResponse = new PermisoResponse();
        permisoResponse.setId(permisoEntity.getId());
        permisoResponse.setNombreEmpleado(permisoEntity.getNombreEmpleado());
        permisoResponse.setFechaPermiso(dateFormat.format(permisoEntity.getFechaPermiso()));
        permisoResponse.setApellidosEmpleado(permisoEntity.getApellidosEmpleado());
        permisoResponse.setTipoPermiso(permisoEntity.getTipoPermiso().toString());

        return permisoResponse;
    }

    @Override
    public void deletePermiso(Long id) {
        permisoRepository.deleteById(id);
    }

    @Override
    public List<TipoPermisoEntity> tipoPermisoList() {
        return tipoPermisoRepository.findAll();
    }
}
